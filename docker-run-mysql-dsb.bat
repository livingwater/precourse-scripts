echo off
echo Starting MySQL, root password=ppp
docker run -p 3306:3306 --name mysql-dsb-populated -e MYSQL_ROOT_PASSWORD=ppp -d -v //c/Users/Selvyn/demo/vm_share/mysql:/root mysql-db-unpopulated:latest
echo MySQL container now running
echo 

echo Waiting for MySQL deamon to initialise, do not interrupt... (CTRL+C will termiate the install)
timeout 30 /NOBREAK

echo Initialising and populating the DB, can take approximately 5 minutes to complete, please wait...
docker exec -it mysql-dsb-populated bash -c "cd /root; ./init-populate-mysql-db-dsb-for-sa.sh"
echo MySQL available for use...
echo  
